#!/usr/bin/env python3

from subprocess import check_output

def passwd(server, username):
    return check_output('keyring get {0} {1}'.format(server, username), shell=True).splitlines()[0]
