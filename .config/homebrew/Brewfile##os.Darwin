# -*- mode: ruby -*-
# Official Taps
tap "homebrew/cask-fonts"
tap "homebrew/cask-drivers"
tap "homebrew/core"
tap "koekeishiya/formulae"

# Third-Party Taps
tap "railwaycat/emacsmacport"
tap "little-angry-clouds/homebrew-my-brews"
tap "warrensbox/tap"

# Essential Brews
brew "coreutils"
brew "curl"
brew "git"
brew "bat"
brew "jq"
brew "homeport/tap/dyff"
brew "asdf"
brew "exa" # todo: configure
brew "httpie"
brew "openfortivpn"
brew "mtr"
brew "railwaycat/emacsmacport/emacs-mac", args: ["HEAD", "with-emacs-big-sur-icon", "with-no-title-bars"]
brew "git-crypt"
brew "gnupg"
brew "pinentry-mac"
brew "cmake"
brew "ripgrep"
brew "kubie"
# brew "kbenv" # does not currenlty work properly with completion 
brew "krew"
brew "warrensbox/tap/tfswitch"
brew "minikube"
brew "hyperkit"
brew "mackup"
brew "pipenv"
brew "nodenv"
brew "git-delta"
brew "myrepos"
brew "skaffold"
brew "helm"
brew "helmfile"
brew "docker"
brew "kustomize"
brew "istioctl"
brew "tig"
brew "stern"
brew "rust" # mostly for installing apps via cargo
brew "yabai" # todo: configure, disable sip
brew "skhd" # todo: see yabai
brew "docker-compose"
brew "ansible-lint"
brew "awscli"
brew "aws-elasticbeanstalk"
brew "linkerd"
brew "syncthing"
brew "tflint"
brew "terraform-docs"

# rust packages installed via cargo. they are not supported here, but recorded for posterity
# crate "drill"

# Testing/Evaluating Brews
brew "whalebrew" # https://github.com/whalebrew/whalebrew

# Essential Casks
cask "kitty"
cask "font-iosevka-nerd-font"
cask "font-jetbrains-mono-nerd-font"
cask "moneymoney"
#cask "visual-studio-code"
cask "aws-vault"
cask "launchbar"
cask "mailmate"
cask "docker"
#cask "bartender" # -> setapp
cask "intellij-idea"
cask "zoom"
cask "virtualbox"
cask "barrier"
#cask "logitech-g-hub"
cask "slack"
cask "1password"

# todo: mas
brew "mas" # enabled `mas` target

mas "clockodo", id: 991892390
